require "minitest/autorun"
require "minitest/mock"
require_relative './set_volley'
class TestSetVolley < Minitest::Test
  def setup
    @set = SetVolley.new(0.5, 0.5, 14)
  end
  def test_player_one_is_serving
    assert_equal false, @set.player_one_is_serving?(1, 19, 15, 0)
    assert_equal true, @set.player_one_is_serving?(17, 22, 15, 0)
    assert_equal false, @set.player_one_is_serving?(17, 22, 15, 1)
  end
  def test_set_finished
    assert_equal true, @set.set_finished?(15, 13, 15)
    assert_equal false, @set.set_finished?(12, 11, 15)
    assert_equal false, @set.set_finished?(17, 16, 15)
  end
  def test_simulate_point
    # mock rand_element return value
    @set.stub :rand_element, 0.4 do
      # first player wins
      assert_equal 0, @set.simulate_point(nil)
    end
    @set = SetVolley.new(0.39, 0.2, 14)
    @set.stub :rand_element, 0.4 do
      # second player wins
      assert_equal 1, @set.simulate_point(nil)
    end
  end

  def test_simulate_set
    @set = SetVolley.new(0.5, 0, 15)
    # mock random generating

    @set.stub :rand_element, 0.4 do
      # second player wins
      assert_equal 15, @set.simulate_set.player_1
      assert_equal 0, @set.simulate_set.player_2
    end
  end

end