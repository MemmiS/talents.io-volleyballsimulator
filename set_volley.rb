require_relative './score'
class SetVolley

  MIN_MARGIN_TO_WIN_SET = 2
  def initialize(p_t1_wins_serving, p_t2_wins_serving, nb_min_points_to_win)
    @p1 = p_t1_wins_serving
    @p2 = p_t2_wins_serving
    @nb_min_points_to_win = nb_min_points_to_win
    @score_player_1 = 0
    @score_player_2 = 0
    @nb_ball_played = 0
  end


  def simulate_set
    last_win = nil
    score = Score.new(@score_player_1, @score_player_2)
    while not set_finished?(@score_player_1, @score_player_2, @nb_min_points_to_win)
      last_win = self.simulate_point(last_win)
      last_win==0?@score_player_1+=1:@score_player_2+=1
      @nb_ball_played += 1
      score.player_1 = @score_player_1
      score.player_2 = @score_player_2
      score.add_score_to_history
    end
    score
  end

  def simulate_point(last_win)
    if player_one_is_serving?(@score_player_1, @score_player_2, @nb_min_points_to_win, @nb_ball_played, last_win) == true
      if rand_element <= @p1
        return 0
      else
        return 1
      end
    else
      if rand_element <= @p2
        return 1
      else
        return 0
      end
    end
  end

  def rand_element
    rand()
  end

  def player_one_is_serving?(score_player_1, score_player_2, nb_min_points_to_win, nb_ball_played, last_win = nil)
    if nb_ball_played == 0
      return ((score_player_2 + score_player_1) / nb_min_points_to_win) % 2 == 0
    else
      return last_win == 1
    end

  end

  def set_finished?(score_player_1, score_player_2, nb_min_points_to_win)
    if (score_player_1 >= nb_min_points_to_win or score_player_2 >= nb_min_points_to_win) and (score_player_1 - score_player_2).abs >= MIN_MARGIN_TO_WIN_SET
      return true
    end
    false
  end
end