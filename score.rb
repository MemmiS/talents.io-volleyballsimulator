class Score
  attr_accessor :player_1, :player_2, :history
  def initialize(player_1, player_2)
    @player_1 = player_1
    @player_2 = player_2
    @history = []
    add_score_to_history
  end

  def add_score_to_history
    @history << self.to_s
  end

  def to_s
    @player_1.to_s + '-' + @player_2.to_s
  end

  def who_won
    @player_1 > @player_2 ? 0 : 1
  end
end