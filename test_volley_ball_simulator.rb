require "minitest/autorun"
require_relative './volley_ball_simulator'
require_relative './score'

class TestVolleyBallSimulator < Minitest::Test
  def test_game_is_finished
    # equality case player 2 wins
    scores = [Score.new(25, 14), Score.new(13,25), Score.new(25, 0), Score.new(0, 25), Score.new(0, 15)]
    assert_equal true, VolleyBallSimulator.new(nil, nil).game_is_finished?(scores)
    # not finished
    scores = [Score.new(16,14)]
    assert_equal false, VolleyBallSimulator.new(nil, nil).game_is_finished?(scores)

    scores = []
    assert_equal false, VolleyBallSimulator.new(nil,nil).game_is_finished?(scores)
  end

  def test_run
    res = VolleyBallSimulator.new(0.99, 0.99).run
    puts res
  end

  def test_format_score
    puts VolleyBallSimulator.new(nil, nil).format_score([Score.new(2,3)])
  end
end