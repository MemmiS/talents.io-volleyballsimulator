require_relative './set_volley'
class VolleyBallSimulator

  def initialize(p1, p2)
    @p1 = p1
    @p2 = p2
  end

  def run
    scores = []
    while not game_is_finished?(scores)
      temp = Hash.new(0)
      scores.each do |score|
        temp[score.who_won] += 1
      end
      number_pts_to_win = ( temp[0] == temp[1] and temp[0]  == 2) ? 15 : 25
      scores << SetVolley.new(@p1, @p2, number_pts_to_win).simulate_set
    end
    format_score(scores)
  end

  def format_score(scores)
    previous_score = ""
    results = []
    scores.each do |score|
      score.history.each do |current_point|
        results << previous_score + current_point
      end
      previous_score += score.player_1.to_s + '-' + score.player_2.to_s + " "
    end
    results
  end

  def game_is_finished?(scores)
    result = Hash.new(0)
    scores.each do |score|
      result[score.who_won] += 1
    end
    result.any? { |_, value| value >= 3}
  end
end